#include <iostream>
#include <SDL_opengles2.h>
#include <vector>

#include "shader.h"

using namespace std;

void Shader::init(const GLchar *fstxt) {
  GLint isCompiled = 0;

  // Create and compile the vertex shader
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource, nullptr);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
  if(isCompiled == GL_FALSE) {
  	GLint maxLength = 0;
  	glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
  	vector<GLchar> infoLog(maxLength);
  	glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);
  	glDeleteShader(vertexShader);
    printf("ERROR: %s\n", &infoLog[0]);
  	return;
  }

  // Create and compile the fragment shader
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fstxt, nullptr);
  glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
  if(isCompiled == GL_FALSE) {
  	GLint maxLength = 0;
  	glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);
  	vector<GLchar> infoLog(maxLength);
  	glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);
  	glDeleteShader(fragmentShader);
    printf("ERROR: %s\n", &infoLog[0]);
  	return;
  }

  // Link the vertex and fragment shader into a shader program
  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);
  glUseProgram(shaderProgram);

  // Specify the layout of the vertex data
  GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

void Shader::prep() {
  glUseProgram(shaderProgram);
  GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

void Shader::debug() {
  printf(" shaderProgram: %d\n", shaderProgram);
  printf("fragmentShader: %d\n", fragmentShader);
  printf("  vertexShader: %d\n", vertexShader);
}
