#include <iostream>
#include <SDL_opengles2.h>

using namespace std;

class Obj2d
{
    public:
      int num_vertices;
      GLfloat *vertices;
      GLfloat *xformed_vertices;
      float x,y, rotate;

    void load_tri();
    void prep();
    void draw();

};
