NAME=es3
emcc Common/esUtil.c ${NAME}.c -s WASM=1 -Os -o ${NAME}.html -s USE_WEBGL2=1

if [[ $? == 0 ]]; then
	emrun --port 8080 ${NAME}.html
else 
	echo "Error building... aborting"
	exit 1
fi
