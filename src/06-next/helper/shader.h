#include <iostream>
#include <SDL_opengles2.h>

using namespace std;

class Shader
{
    public:
      GLuint vertexShader;
      GLuint fragmentShader;
      GLuint shaderProgram;
      const GLchar* vertexSource =
          "attribute vec4 position;                     \n"
          "void main()                                  \n"
          "{                                            \n"
          "  gl_Position = vec4(position.xyz, 1.0);     \n"
          "}                                            \n";


    void init(const GLchar *fstxt);
    void prep();
    void debug();

};
