#include <iostream>
#include <SDL_opengles2.h>
#include "math.h"

#include "obj2d.h"

using namespace std;

void Obj2d::load_tri() {
  num_vertices = 3;
  vertices = new GLfloat[2*num_vertices];
  xformed_vertices = new GLfloat[2*num_vertices];
  vertices[0] =  0.0f; vertices[1] =  0.5f;
  vertices[2] =  0.5f; vertices[3] = -0.5f;
  vertices[4] = -0.5f; vertices[5] = -0.5f;
  x=0.0f;
  y=0.0f;
  rotate = 0.0f;
}

void Obj2d::prep() {

  // Transform rotate around Z
  for (int i=0; i<num_vertices*2; i+=2) {
    float x1 = vertices[i];
    float y1 = vertices[i+1];
    xformed_vertices[i]   = x1*cos(rotate) - y1*sin(rotate);
    xformed_vertices[i+1] = x1*sin(rotate) + y1*cos(rotate);
  }

  // Transform x & y
  for (int i=0; i<num_vertices*2; i+=2) {
    xformed_vertices[i]=xformed_vertices[i]+x;
    xformed_vertices[i+1]=xformed_vertices[i+1]+y;
  }


}

void Obj2d::draw() {

  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*num_vertices*2, xformed_vertices, GL_STATIC_DRAW);

  glDrawArrays(GL_TRIANGLES, 0, 3);
}
