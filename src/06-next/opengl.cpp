// https://github.com/timhutton/opengl-canvas-wasm

#include <functional>
#include <emscripten.h>
#include <SDL.h>
#include <time.h>

#define GL_GLEXT_PROTOTYPES 1
#include <SDL_opengles2.h>

#include "helper/obj2d.h"
#include "helper/shader.h"

Obj2d tri1;
Obj2d tri2;
Shader my_shader1;
Shader my_shader2;
int frame;
float elapsedTime;

const GLchar* fragmentSource =
    "precision mediump float;\n"
    "void main()                                  \n"
    "{                                            \n"
    "  gl_FragColor[0] = gl_FragCoord.x/640.0;    \n"
    "  gl_FragColor[1] = 0.5;   				  \n"
    "  gl_FragColor[2] = 0.5;                     \n"
    "}                                            \n";
    const GLchar* fragmentSource_red =
        "precision mediump float;\n"
        "void main()                                  \n"
        "{                                            \n"
        "  gl_FragColor[0] = 1.0;                  \n"
        "  gl_FragColor[1] = 0.0;   				        \n"
        "  gl_FragColor[2] = 0.0;                     \n"
        "}                                            \n";


void main_loop(void *window)
{

	clock_t start = clock();

    tri1.prep();
    tri2.prep();

    // Clear the screen
  	glClearColor(0.3f, 0.2f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    my_shader2.prep();
    tri1.draw();


    my_shader1.prep();
    tri2.draw();

    SDL_GL_SwapWindow((SDL_Window *)window);

    tri1.rotate = tri1.rotate + 0.01f;
    tri2.rotate = tri2.rotate - 0.02f;

    frame++;

  	clock_t stop = clock();

  	// Every 100th frame show some details about timings
  	elapsedTime += (float)(stop - start) / (float)CLOCKS_PER_SEC * 1000.0f;
  	if (frame%100 == 0) {
  		printf("ElapsedTime (mean): %.1fms (%.0f fps)\n", elapsedTime/100, 100/(elapsedTime/1000));
  		elapsedTime = 0;
  	}

}

int main()
{
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_CreateWindowAndRenderer(640, 480, 0, &window, &renderer);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

  my_shader1.init(fragmentSource);
  my_shader1.debug();

  my_shader2.init(fragmentSource_red);
  my_shader2.debug();

  tri1.load_tri();
  tri2.load_tri();
  tri1.x = -0.6f;
  tri2.x = 0.6f;

  frame = 0;
	elapsedTime = 0;

  emscripten_set_main_loop_arg(main_loop, (void *)window, 60, true);

  return EXIT_SUCCESS;
}
