#include <iostream>
#include <SDL_opengles2.h>

using namespace std;

class Starfield
{
  public:
    int num_points;
    GLfloat *points;
    
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;
    const GLchar* vertexSource =
      "attribute vec4 position;                     \n"
      "void main()                                  \n"
      "{                                            \n"
      "  gl_Position = vec4(position.xyz, 1.0);     \n"
      "  gl_PointSize = 5.0;                        \n"
      "}                                            \n";
    const GLchar* fragmentSource =
      "precision mediump float;                   \n"
      "uniform vec4 ourColour;                    \n"
      "void main() {                              \n"
      "  gl_FragColor = ourColour;                \n"
      "}                                          \n";
    
    void init();
    void update();
    void draw();
    void debug();

};
