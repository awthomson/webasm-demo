#include <iostream>
#include <SDL_opengles2.h>
#include <vector>
#include <random>

#include "starfield.h"

using namespace std;

const int NUM_STARS = 2;

void Starfield::init() {
    
	GLint isCompiled = 0;

	// Create and compile the vertex shader
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
	if(isCompiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
		vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);
		glDeleteShader(vertexShader);
	printf("ERROR: %s\n", &infoLog[0]);
		return;
	}

	// Create and compile the fragment shader
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
	if(isCompiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);
		vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);
		glDeleteShader(fragmentShader);
	printf("ERROR: %s\n", &infoLog[0]);
		return;
	}

	// Link the vertex and fragment shader into a shader program
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);    
    
    num_points = NUM_STARS;
    points = new GLfloat[4*num_points];
    for (int i=0; i < num_points*4; i=i+4) {
    	float x = (rand() % 100-50)/100.0f;		// -0.5 -> +0.5 
    	float y = (rand() % 100-50)/100.0f;		// -0.5 -> +0.5
    	float z = ((rand() % 50)+50)/100.0f;		//  0.0 -> +1.0
    	float a = 0.0f;
    	points[i] = x;
    	points[i+1] = y;
    	points[i+2] = z;
    	points[i+3] = a;
    }
}

void Starfield::update() {
    for (int i=0; i < num_points*4; i=i+4) {
    	float z= points[i+2];
    	points[i+1] -= (z/100.0f);
    	if (points[i+1] < -2.0f) {
    		points[i+1] =  2.0f;
    	}
    }	
}

void Starfield::draw() {

	glUseProgram(shaderProgram);

	GLint vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColour");
	glUniform4f(vertexColorLocation, 1.0f, 1.0f, 1.0f, 1.0f);

	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 2, 0);
	
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*num_points*4, points, GL_STATIC_DRAW);

    glDrawArrays(GL_POINTS, 0, 2);	
}


void Starfield::debug() {
    for (int i=0; i < num_points*4; i=i+4) {
    	float x = points[i];
    	float y = points[i+1];
    	float z = points[i+2];
    	float a = points[i+3];
    	printf("X=%2.3f   Y=%2.3f   Z=%2.3f   A=%2.3f\n", x, y, z, a);
    }	
}
