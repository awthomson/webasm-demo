// https://github.com/timhutton/opengl-canvas-wasm

#include <functional>
#include <emscripten.h>
#include <SDL.h>
#include <time.h>

#define GL_GLEXT_PROTOTYPES 1
#include <SDL_opengles2.h>

#include "helper/obj2d.h"
#include "helper/shader.h"
#include "extra/starfield.h"

Obj2d tri1;
Shader my_shader1;
Starfield my_stars;
int frame;
float elapsedTime;

const GLchar* fragmentSource =
    "precision mediump float;                   \n"
    "uniform vec4 ourColour;                    \n"
    "void main() {                              \n"
    "  gl_FragColor = ourColour;                \n"
    "}                                          \n";


void main_loop(void *window)
{

	clock_t start = clock();

    tri1.prep();

    // Clear the screen
  	glClearColor(0.3f, 0.2f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    /*
    my_shader1.prep();
    tri1.draw();
    */
    my_stars.update();
    my_stars.debug();
    my_stars.draw();
    

    SDL_GL_SwapWindow((SDL_Window *)window);

    tri1.rotate = tri1.rotate + 0.01f;

    frame++;

  	clock_t stop = clock();

  	// Every 100th frame show some details about timings
  	elapsedTime += (float)(stop - start) / (float)CLOCKS_PER_SEC * 1000.0f;
  	if (frame%100 == 0) {
  		printf("ElapsedTime (mean): %.1fms (%.0f fps)\n", elapsedTime/100, 100/(elapsedTime/1000));
  		elapsedTime = 0;
  	}

}

int main()
{
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_CreateWindowAndRenderer(640, 480, 0, &window, &renderer);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	
	my_stars.init();
	my_stars.debug();

	my_shader1.init(fragmentSource);
	my_shader1.debug();

	tri1.load_tri();

	frame = 0;
	elapsedTime = 0;

	emscripten_set_main_loop_arg(main_loop, (void *)window, 10, true);

	return EXIT_SUCCESS;
}
