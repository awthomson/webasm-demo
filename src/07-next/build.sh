NAME=next
emcc extra/*.cpp helper/*.cpp ${NAME}.cpp -s WASM=1 -o ${NAME}.html  -std=c++11 -s USE_SDL=2
if [[ $? == 0 ]]; then
	emrun --port 8080 ${NAME}.html
else
	echo "Error building... aborting"
	exit 1
fi
