#include <emscripten/emscripten.h>
#include <emscripten/bind.h>
// using namespace emscripten;

#define GLFW_INCLUDE_ES3

#include <GLFW/glfw3.h>
#include <GLES3/gl3.h>


void initialize()
{
 // Initialise GLFW
    if( !glfwInit() )
    {

        EM_ASM_({ console.log(' failed: '); }, 0);  
	}

    EM_ASM_({
      console.log(' end initialization: ');
    }, 0);  
}
EMSCRIPTEN_BINDINGS(glfw) 
{   
   // function("initialize", &initialize);
}
