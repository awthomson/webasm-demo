#include <emscripten/emscripten.h>
#include <emscripten/bind.h>

#define GLFW_INCLUDE_ES3
#include <GLFW/glfw3.h>
#include <GLES3/gl3.h>
using namespace emscripten;

int main()
{
 // Initialise GLFW
    if( !glfwInit() )
    {

        EM_ASM_({
          console.log(' failed: ');
        }, 0);  
    }

    EM_ASM_({
      console.log(' end initialization: ');
    }, 0);  

    return 0;
}
