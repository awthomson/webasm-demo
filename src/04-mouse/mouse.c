#include <stdio.h>
#include <SDL/SDL.h>
#include <emscripten.h>
#include <time.h>

const int SCREEN_WIDTH = 320;
const int SCREEN_HEIGHT = 256;

SDL_Surface *screen;
int frame;
float elapsedTime;
int xpos=100;
int ypos=100;

void loop(void);
void drawPixel(int x, int y, int r, int g, int b);
void clearBuffer(void);

int main(int argc, char** argv) {

	SDL_Init(SDL_INIT_VIDEO);
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE);

	frame = 0;
	elapsedTime = 0;

	emscripten_set_main_loop(loop, 0, 0);

	return 0;
}

void loop(void) {
	clock_t start = clock();

	SDL_Event event;
	if (SDL_PollEvent(&event)) {
		switch(event.type) {
 		/* Mouse event */
		case SDL_MOUSEBUTTONDOWN:
    			switch (event.button.button) {
			case SDL_BUTTON_LEFT:
            			printf("Left mouse button clicked\n");
           			break;
			case SDL_BUTTON_RIGHT:
       				printf("Right mouse button clicked\n");
       				break;
			default:
				break;
			}
		case SDL_MOUSEMOTION:
			xpos = event.motion.x;
			ypos = event.motion.y;
			if (xpos < 0) xpos = 0;
			if (xpos > SCREEN_WIDTH) xpos = SCREEN_WIDTH;
			if (ypos < 0) ypos = 0;
			if (xpos > SCREEN_HEIGHT) ypos = SCREEN_HEIGHT;
			break;
		default:
			break;
		}
	}

	if (SDL_MUSTLOCK(screen))
		SDL_LockSurface(screen);

	// Clear display
	clearBuffer();

	// Draw user
	for (int x=0; x<SCREEN_WIDTH; x++)
      		drawPixel(x, ypos, 128, 128, 128);
	for (int y=0; y<SCREEN_HEIGHT; y++)
      		drawPixel(xpos, y, 128, 128, 128);
	drawPixel(xpos, ypos, 255, 255, 255);

	if (SDL_MUSTLOCK(screen))
		SDL_UnlockSurface(screen);

	SDL_Flip(screen);
	frame++;

	clock_t stop = clock();

	// Every 100th frame show some details about timings
	elapsedTime += (float)(stop - start) / (float)CLOCKS_PER_SEC * 1000.0f;
	if (frame%100 == 0) {
		printf("ElapsedTime (mean): %.1fms (%.0f fps)\n", elapsedTime/100, 100/(elapsedTime/1000));
		printf("X=%d, Y=%d\n", xpos, ypos);
		elapsedTime = 0;
	}
}

void clearBuffer(void) {
	for (int y = 0; y < SCREEN_HEIGHT; y++) {
		for (int x = 0; x < SCREEN_WIDTH; x++) {
      			drawPixel(x, y, 0, 0, 0);
    		}
  	}
}

void drawPixel(int x, int y, int r, int g, int b) {
      	*((Uint32*)screen->pixels + y * SCREEN_WIDTH + x) = SDL_MapRGBA(screen->format, r, g, b, 255);
}
