emcc sdl.c -s WASM=1 -o sdl.html 
if [[ $? == 0 ]]; then
	emrun --port 8080 sdl.html
else 
	echo "Error building... aborting"
	exit 1
fi
