#include <stdio.h>
#include <SDL/SDL.h>
#include <emscripten.h>
#include <time.h>

SDL_Surface *screen;
int frame;
float elapsedTime;

void loop(void);

int main(int argc, char** argv) {
		
	SDL_Init(SDL_INIT_VIDEO);
	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);

	frame = 0;
	elapsedTime = 0;
	
	emscripten_set_main_loop(loop, 120, 0);
	
	return 0;
}

void loop(void) {
	clock_t start = clock();

	if (SDL_MUSTLOCK(screen))
		SDL_LockSurface(screen);

	// Draw a simple image to buffer, pixel-by-pixel
	for (int y = 0; y < 480; y++) {
		for (int x = 0; x < 640; x++) {
			// NOTE: Alpha is not used here
      			*((Uint32*)screen->pixels + y * 640 + x) = SDL_MapRGBA(screen->format, y>>1, frame%255, 128, 255);
    		}
  	}

	if (SDL_MUSTLOCK(screen))
		SDL_UnlockSurface(screen);
	
	SDL_Flip(screen); 
	frame++;

	clock_t stop = clock();

	// Every 100th frame show some details about timings
	elapsedTime += (float)(stop - start) / (float)CLOCKS_PER_SEC * 1000.0f;
	if (frame%100 == 0) {
		printf("Frame: %d\n", frame);
		printf("ElapsedTime (mean): %.1fms (%.0f fps)\n", elapsedTime/100, 100/(elapsedTime/1000));
		elapsedTime = 0;
	}
}

